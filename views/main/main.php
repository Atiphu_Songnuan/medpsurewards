<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ประกาศผลรางวัล</title>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Theme style -->
  <link rel="icon" type="image/png" href="public/img/fireworks.png" />
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="public/css/adminlte.min.css">
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-default@3/default.css" rel="stylesheet" />

  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="https://kit.fontawesome.com/b7a24e7c0e.js" crossorigin="anonymous"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
  <script src="public/js/scripts.min.js"></script>
  <script src="public/js/adminlte.js"></script>
  <!-- *Datatable -->
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Niramit:wght@500&display=swap');

    body {
      font-family: 'Niramit', sans-serif;
    }

    .background-header {
      background: #ee9ca7;
      /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #ffdde1, #ee9ca7);
      /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #ffdde1, #ee9ca7);
      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }
  </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
  <div class="wrapper">

    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
      <img class="animation__wobble" src="public/img/medpsu.png" alt="medpsu" height="60" width="60">
    </div>


    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a onclick="onBackToMain()" class="nav-link text-primary"><i class="fas fa-chevron-circle-left"></i> กลับ</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li> -->
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="#" class="brand-link">
        <img src="public/img/medpsu.png" alt="medpsu Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
        <span class="brand-text font-weight-light">คณะแพทยศาสตร์</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a onclick="GetAllRewards('ทองคำรูปพรรณ 2 สลึง')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/gold1.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  ทองคำรูปพรรณ 2 สลึง
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('ทองคำรูปพรรณ 1 สลึง')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/gold2.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  ทองคำรูปพรรณ 1 สลึง
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เงินรางวัล 2,000 บาท')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/dollar.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เงินรางวัล 2,000 บาท
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เงินรางวัล 1,000 บาท')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/dollar.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เงินรางวัล 1,000 บาท
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เงินรางวัล 500 บาท')" class="nav-link">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/dollar.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เงินรางวัล 500 บาท
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เงินรางวัล 200 บาท')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/dollar.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เงินรางวัล 200 บาท
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เตาแม่เหล็กไฟฟ้า')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/stove.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เตาแม่เหล็กไฟฟ้า
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เตาปิ้งย่าง')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/grill.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  <p>
                    เตาปิ้งย่าง
                    <!-- <span class="right badge badge-danger">New</span> -->
                  </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เตาย่างไฟฟ้าแบบตั้งโต๊ะ')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/grill.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  <p>
                    เตาย่างไฟฟ้าแบบตั้งโต๊ะ
                    <!-- <span class="right badge badge-danger">New</span> -->
                  </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('หม้อหุงข้าว')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/rice-cooker.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  <p>
                    หม้อหุงข้าว
                    <!-- <span class="right badge badge-danger">New</span> -->
                  </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('ไมโครเวฟ')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/oven.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  <p>
                    ไมโครเวฟ
                    <!-- <span class="right badge badge-danger">New</span> -->
                  </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('พัดลมสไลด์')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/fan.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  พัดลมสไลด์
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เครื่องฟอกอากาศ')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/air.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เครื่องฟอกอากาศ
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เตารีดไอน้ำ')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/iron.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เตารีดไอน้ำ
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เครื่องดูดฝุ่น')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/vacuum.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เครื่องดูดฝุ่น
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('หม้อทอดไร้น้ำมัน')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/pot.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  หม้อทอดไร้น้ำมัน
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เตาปิ้งย่าง-ชาบู')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/grill.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เตาปิ้งย่าง-ชาบู
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a onclick="GetAllRewards('เครื่องชั่งวัดไขมัน')" class="nav-link" style="cursor: pointer;">
                <!-- <i class="nav-icon fas fa-th"></i> -->
                <img src="public/img/scale.png" alt="medpsu Logo" width="24" height="24"
                  class="brand-image img-circle elevation-3" style="opacity: .8">
                <p>
                  เครื่องชั่งวัดไขมัน
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">ประกาศผลรางวัล</h1>
            </div><!-- /.col -->
            <!-- <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard v2</li>
              </ol>
            </div> -->
            <!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
              <!-- TABLE: LATEST ORDERS -->
              <div class="card p-3">
                <div class="card-header border-transparent">
                  <h3 class="card-title">รายชื่อผู้โชคดีที่ได้รับ <span class="m-l-3 text-danger"
                      style="font-size: 16px; font-weight: bold;" id="rewardtitle"></span></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table m-3" id="rewardstable" style="white-space:nowrap;">
                      <thead>
                        <tr>
                          <th>ลำดับ</th>
                          <th>รหัสบุคลากร</th>
                          <th>ชื่อ-สกุล</th>
                          <th>หน่วยงาน</th>
                          <th>รางวัล</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <!-- <div class="card-footer clearfix">
                  <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                  <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
                </div> -->
                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!--/. container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer text-center" style="font-size: 14px;">
      ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์
      โรงพยาบาลสงขลานครินทร์
    </footer>
  </div>
</body>

</html>