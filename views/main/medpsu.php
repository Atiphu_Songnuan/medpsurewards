<?php header('Content-Type: text/html; charset=utf-8');?>
<!DOCTYPE html>
<html lang="th" translate="no">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="Content-Language" content="th">
  <!-- <meta http-equiv="content-Type" content="text/html; charset=tis-620"> -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" type="image/png" href="public/img/fireworks.png" />
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" />
  <link rel="stylesheet" href="bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" />
  <!-- <link rel="stylesheet" href="public/css/define.css">
  <link rel="stylesheet" href="public/css/but.css"> -->
  <!-- <link rel="stylesheet" href="public/css/styles.css" /> -->
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-default@3/default.css" rel="stylesheet" />

  <title>ประกาศผลรางวัล</title>
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="https://kit.fontawesome.com/b7a24e7c0e.js" crossorigin="anonymous"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

  <!-- <script src="public/js/qrcode.js"></script> -->
  <script src="public/js/scripts.js"></script>
  <!-- <script src="public/js/payment.min.js"></script> -->
  <script src="https://www.google.com/recaptcha/api.js"></script>

  <!-- *Datatable -->
  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <!-- <script src="bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script> -->
  <script src="bower_components/datatables.net-select/js/dataTables.select.min.js"></script>

  <!-- QR Code Scanner -->
  <!-- <script src="public/js/html5-qrcode.min.js"></script> -->
  <!-- <script src="public/js/qrcode/filereader.js"></script>
  <script src="public/js/qrcode/qrcodelib.js"></script>
  <script src="public/js/qrcode/webcodecamjs.js"></script>
  <script src="public/js/qrcode/main.js"></script> -->


  <style>
    html {
      position: relative;
      min-height: 100%;
    }

    body {
      padding-top: 0;
      padding-left: 0;
      margin: 0;
      font-family: 'Niramit', sans-serif;
      background-color: #eee;
      margin-bottom: 60px;
    }

    .background-gradient {
      background: #FFAFBD;
      /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #ffc3a0, #FFAFBD);
      /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #ffc3a0, #FFAFBD);
      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }

    .background-header {
      background: #0F2027;
      /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #2C5364, #203A43, #0F2027);
      /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #2C5364, #203A43, #0F2027);
      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    }

    label,
    span a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5 {
      font-family: 'Niramit', sans-serif;
      /* font-size: 1.1em; */
      font-weight: 300;
      line-height: 1.7em;
      color: black;
    }

    .swal2-footer {
      text-align: center;
    }

    body.swal2-height-auto {
      height: 100% !important
    }

    .swal2-styled.swal2-cancel {
      font-family: 'Niramit', sans-serif;
    }

    th.dt-center,
    td.dt-center {
      text-align: center;
    }

    .footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      background-color: #1e4b25;
    }

    body>.container {
      padding: 60px 15px 0;
    }

    .footer>.container {
      padding-right: 15px;
      padding-left: 15px;
    }

    .block {
      display: block;
      width: 100%;
      border: none;
      padding: 14px 28px;
      font-size: 16px;
      text-align: center;
    }


    @media only screen and (max-width: 768px) {
      .card {
        width: 100%;
      }

      .clinic-card {
        width: 100%;
      }

      .bg {
        height: auto;
      }
    }
  </style>
</head>

<body>
  <header>
    <!-- Fixed navbar -->
    <div class="navbar shadow-sm background-gradient">
      <div class="container d-flex justify-content-between">
        <a class="navbar-brand text-white d-flex align-items-center">
          <!-- <img src="public/img/medpsu.png" width="50" height="60" class="d-inline-block align-top" alt=""> -->
          <span class="ml-3">ประกาศผลรางวัล</span>
        </a>
      </div>
    </div>
  </header>

  <!-- Begin page content -->
  <main role="main" class="p-3 container">
    <div class="card mx-auto shadow p-3 mb-5 bg-white rounded-lg">
      <div class="card-header text-center background-header text-white">Header Card</div>
      <div class="card-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">รหัสบุคลากร</th>
              <th scope="col">ชื่อ-สกุล</th>
              <th scope="col">รางวัล</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">11111</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
            <tr>
              <th scope="row">22222</th>
              <td>Jacob</td>
              <td>Thornton</td>
            </tr>
            <tr>
              <th scope="row">33333</th>
              <td>Larry</td>
              <td>the Bird</td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- <div class="card-footer">Footer Card</div> -->
    </div>

    <div class="card mx-auto mt-3 shadow p-3 mb-5 bg-white rounded-lg">
      <div class="card-header text-center background-header text-white">Header Card</div>
      <div class="card-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">รหัสบุคลากร</th>
              <th scope="col">ชื่อ-สกุล</th>
              <th scope="col">รางวัล</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">11111</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
            <tr>
              <th scope="row">22222</th>
              <td>Jacob</td>
              <td>Thornton</td>
            </tr>
            <tr>
              <th scope="row">33333</th>
              <td>Larry</td>
              <td>the Bird</td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- <div class="card-footer">Footer Card</div> -->
    </div>
  </main>

  <footer class="p-2 text-center footer background-gradient">
    <div class="container">
      <span class="text-white">ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์ <br />โรงพยาบาลสงขลานครินทร์</span>
    </div>
  </footer>

</body>

</html>