let rewardstable;
let getRewardInterval;
$(document).ready(function () {
  GetAllRewards("ทองคำรูปพรรณ 2 สลึง");
});

function GetAllRewards(giftname) {
  SetDataTable([]);
  // console.log("รางวัล" + giftname);
  $("#rewardtitle").text(giftname);

  CallAPIRewards(giftname);
  // clearInterval(getRewardInterval);
  // getRewardInterval = setInterval(() => {
  //   console.log("Loop get all reward" + giftname);
  //   CallAPIRewards(giftname);
  // }, 5000);
}

function CallAPIRewards(giftname) {
  let body = {
    action: "getallrewards",
    data: {
      gift: giftname,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      // ConnectionFailed();
    },
    success: (response) => {
      let rewardlist = [];

      // !For Design DB
      // if (response.statusCode === 200) {
      //   let resData = JSON.parse(response.data);
      //   let rewardLongData = "";
      //   if (resData.length != 0) {
      //     resData.forEach((element) => {
      //       // console.log(element);
      //       rewardLongData +=
      //         element.PERID + " " + element.NAME + " " + element.SURNAME + " | ";
      //       rewardlist.push({
      //         perid: element.PERID,
      //         fullname: element.NAME + " " + element.SURNAME,
      //         gift: `<span class="badge badge-danger" style="font-size:18px;"><i class="fas fa-gift"></i> ${element.GIFT}</span>`,
      //       });
      //     });
      //     SetDataTable(rewardlist);
      //   } else {
      //     SetDataTable([]);
      //   }
      //   $("#rewardsrunning").text("รางวัล" + giftname + " " + rewardLongData);
      // }

      // !For His-Test DB
      let res = JSON.parse(response);
      // console.log(res);
      if (res.statusCode === 200) {
        // let rewardLongData = "";
        if (res.data.length != 0) {
          let rewarddata = res.data;
          // let rewardLongData = "";
          rewarddata.forEach((element, index) => {
            // console.log(element);
            // rewardLongData +=
            //   element.PERID + " " + element.NAME + " " + element.SURNAME;
            rewardlist.push({
              no: index + 1,
              perid: element.PERID,
              fullname: element.NAME + " " + element.SURNAME,
              depart: element.DEPART,
              gift: `<span class="badge badge-danger" style="font-size:18px;"><i class="fas fa-gift"></i> ${element.GIFT}</span>`,
            });
          });
          // console.log(giftname);
          // console.log(rewardLongData);
          SetDataTable(rewardlist);
        } else {
          SetDataTable([]);
        }
        // $("#rewardsrunning").text("รางวัล" + giftname + " " + rewardLongData);
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

function SetDataTable(tabledata) {
  rewardstable = $("#rewardstable").DataTable({
    // "scrollX": true,
    searching: true,
    responsive: true,
    // bPaginate: false,
    // ordering: false,
    // destroy: true,
    // info: false,
    // order: [[0, "desc"]],
    // bLengthChange: false,
    // bAutoWidth: false,
    // aLengthMenu: [25, 50, 100],
    // iDisplayLength: 25,

    bPaginate: true,
    ordering: false,
    destroy: true,
    info: true,
    order: [[0, "desc"]],
    aLengthMenu: [25, 50, 100],
    iDisplayLength: 25,
    data: tabledata,
    columns: [
      { data: "no" },
      { data: "perid" },
      { data: "fullname" },
      { data: "depart" },
      { data: "gift" },
    ],
    columnDefs: [
      {
        className: "text-center",
        targets: 0,
      },
      // {
      //   className: "badge badge-success",
      //   targets: 2,
      // },
    ],
  });
}

function onBackToMain() {
  window.history.back();
}
