function selectedGift(gift) {
    // console.log(gift);
    GetAllRewards(gift);
    // setInterval(() => {
    //   console.log("Get All Gift Data: " + gift);
    //   GetAllRewards();
    // }, 5000);
  }
  
  function GetAllRewards(gift) {
    let rewardBody = {
      action: "getallrewards",
      data: {
        gift: gift,
      },
    };
  
    $.ajax({
      // url:"https://his01.psu.ac.th/HosApp/trackingdrugs/libs/SendMail.php", //the page containing php script
      url: "/libs/ApiService.php", //the page containing php script
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      type: "POST", //request type,
      data: JSON.stringify(rewardBody),
      // data: jsondata,
      success: (response) => {
        console.log(response);
        // let res = JSON.parse(response);
        // console.log(res);
        // let jsondata = [];
        // res.forEach((element, index) => {
        //   jsondata.push({
        //     pack_perid: element.PACKPERID,
        //     order_id: element.ORDERID,
        //     hn: element.HN,
        //     fullname: element.FULLNAME,
        //     address: element.ADDRESS,
        //     postal: element.POSTAL,
        //     phone: element.PHONE,
        //     doc_no: element.DOCNO,
        //     box_id: element.BOXID,
        //     box_size: element.BOXSIZE,
        //     box_status: element.BOXSTATUS,
        //     senddate: element.SENDDATE,
        //     status: element.STATUS,
        //     productvalue: element.PRODUCTVALUE,
        //   });
        // });
        // table = $("#drughometrackingtable").DataTable({
        //   // "scrollX": true,
        //   bPaginate: true,
        //   ordering: true,
        //   destroy: true,
        //   info: true,
        //   order: [[0, "desc"]],
        //   aLengthMenu: [25, 50, 100],
        //   iDisplayLength: 25,
        //   data: jsondata,
        //   columns: [
        //     { data: "pack_perid" },
        //     { data: "order_id" },
        //     { data: "hn" },
        //     { data: "fullname" },
        //     { data: "address" },
        //     { data: "postal" },
        //     { data: "phone" },
        //     { data: "doc_no" },
        //     { data: "box_id" },
        //     { data: "box_size" },
        //     { data: "box_status" },
        //     { data: "senddate" },
        //     { data: "status" },
        //     { data: "productvalue" },
        //   ],
        //   columnDefs: [
        //     {
        //       targets: [3, 4, 5, 6, 10, 13],
        //       visible: false,
        //     },
        //     {
        //       targets: 0, // this means controlling cells in column 1
        //       render: function (data, type, row, meta) {
        //         return (
        //           "<div class='badge badge-pill badge-info p-2' style='width:auto; font-size:16px;'>" +
        //           data +
        //           "</div>"
        //         );
        //       },
        //     },
        //     {
        //       targets: 7, // this means controlling cells in column 1
        //       render: function (data, type, row, meta) {
        //         return (
        //           "<div class='badge badge-pill badge-warning p-2' style='font-size:16px; color:black;'>" +
        //           data +
        //           "</div>"
        //         );
        //       },
        //     },
        //     {
        //       targets: 12, // this means controlling cells in column 1
        //       render: function (data, type, row, meta) {
        //         return (
        //           "<div class='badge badge-pill badge-warning p-2' style='font-size:16px; color:black;'>" +
        //           data +
        //           "</div>"
        //         );
        //       },
        //     },
        //   ],
        // });
        // onUnSelectAll();
      },
    });
  }
  