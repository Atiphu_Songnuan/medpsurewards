<?php
class Running
{
    public function __construct()
    {
        $this->views = new View();
        // $this->loadModel($name);
    }

    public function index()
    {
        $this->views->render('running/index');
    }

}
