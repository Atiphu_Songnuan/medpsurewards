<?php
class NotFound
{
    public function __construct()
    {
        $this->views = new View();
        // $this->loadModel($name);
    }

    public function index()
    {
        $this->views->msg = 'This page does not exists.';
        $this->views->render('notfound/index');
    }

}
