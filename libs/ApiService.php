<?php
header('Content-Type: text/html; charset=utf-8');
$postdata = file_get_contents("php://input");
if ($postdata != "") {
    # code...
    $request = json_decode($postdata);
    switch ($request->action) {
        case 'getallrewards':
            $rewards = new ApiService();
            $rewards->GetAllRewards($request->data);
            break;
        default:
            # code...
            break;
    }
}

class ApiService
{
    // *Production
    // public $dsn = "mysql:host=192.168.1.13;charset=utf8;dbname=OPD";
    // public $user = "AppServ";
    // public $passwd = "*AppServ*";

    // *For Test Design DB
    // public $dsn = "mysql:host=172.29.1.126;charset=utf8;dbname=party_db";
    // public $user = "atiphu.s";
    // public $passwd = "#$@TIpHu.S@126$#";

    // *For HIS-Test DB
    public $dsn = "mysql:host=172.29.1.157;charset=utf8;dbname=party_db";
    public $user = "doitadm";
    public $passwd = "adm@doit";

    public function GetAllRewards($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $gift = $data->gift;
        // $length = $data->length;
        $sql = "SELECT
                    A.PERID,
                    B.`NAME`,
                    B.SURNAME,
                    C.Dep_name AS DEPART,
                    A.GIFT
                FROM
                    party_db.tbl_register AS A
                    INNER JOIN
                    STAFF.Medperson AS B
                    ON
                        A.PERID = B.PERID
                    INNER JOIN
                    STAFF.Depart AS C
                    ON
                        B.DEP_WORK = C.Dep_Code
                WHERE
                    A.GIFT = '" . $gift . "'
                ORDER BY
                    A.PERID -- GET ALL Reward";

        // !For Design DB
        // $sql = $this->UTF8_TO_TIS620($sql);
        // $path = __DIR__ . "/../logs/log-" . date("Y-m-d") . ".txt";
        // $newRequestData = $sql . PHP_EOL;
        // @file_put_contents($path, $newRequestData, FILE_APPEND);
        // $this->Query($sql);

        // !For His-Test DB
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function Query($sql)
    {
        $sql = urlencode($sql);
        // 192.168.1.33 for HIS-Real
        // $url = "http://192.168.1.33/his/gen12c.php?sql=" . $sql;
        // 172.29.1.4 for HIS-Test
        $url = "http://172.29.1.4/his-test/gen12c.php?sql=" . $sql;

        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        // $result = file_get_contents($url, false, $context);
        $head = array();
        foreach ($http_response_header as $k => $v) {
            $t = explode(':', $v, 2);
            if (isset($t[1])) {
                $head[trim($t[0])] = trim($t[1]);
            } else {
                $head[] = $v;
                if (preg_match("#HTTP/[0-9\.]+\s+([0-9]+)#", $v, $out)) {
                    $head['reponse_code'] = intval($out[1]);
                }
            }
        }
        $columns = explode("\\u0001", $head["Columns"]);
        $data = explode(chr(2), $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode(chr(1), $v);
                $itemArr = array();
                foreach ($columns as $index => $name) {
                    $itemArr[$name] = $line[$index];
                }
                array_push($table, $itemArr);
            }
        }

        header("Content-type:application/json");
        $jsondata = json_encode($table);
        $result = array(
            "statusCode" => 200,
            "data" => $jsondata,
        );
        echo json_encode($result);
    }

    public function UTF8_TO_TIS620($sql)
    {
        return iconv("UTF-8", "TIS-620", $sql);
    }

    public function TIS620_TO_UTF8($sql)
    {
        return iconv("TIS-620", "UTF-8", $sql);
    }
}
